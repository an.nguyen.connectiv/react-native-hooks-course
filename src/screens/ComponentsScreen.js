import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native'

const styles = StyleSheet.create({
    textStyle: {
        color: 'red'
    }
})
export default class ComponentsScreen extends Component {
    render() {
        const name = 'DBand';
        return (
            <View>
                <Text style={styles.textStyle}> textInComponent using css {name}</Text>
            </View>
        )
    }
}
