import React from 'react'
import { FlatList, Text } from 'react-native'

export default function ListScreen() {
    const DATA = [
        {
          id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
          title: 'First Item',
          age: 15
        },
        {
          id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
          title: 'Second Item',
          age: 14

        },
        {
          id: '58694a0f-3sda1-471f-bd96-145571e29d72',
          title: 'Third Item',
          age: 13

        },{
          id: '58694a0f-s3da1-471f-bd96-145571e29d72',
          title: 'Third Item',
          age: 13

        },{
          id: '58694a0f-3da1f-471f-bd96-145571e29d72',
          title: 'Third dsfsdfsdfsdfsd',
          age: 12

        },
      ];
    return (
        <FlatList
          vertical
          keyExtractor={DATA=>DATA.id} 
          data={DATA}
          renderItem={ ({item}) => {
            return <Text>Title: {item.title} - Page: {item.age}</Text>;
          }}
        />
    )
}
