import React from 'react'
import { Button, Text, TouchableOpacity, View } from 'react-native'


export default function HomeScreen(props) {
    const test=  'Test'
    console.log(props);
    return (
        <View>
            <Text>Hello World {test}</Text>
            <Button 
            onPress={() => props.navigation.navigate('ListScreen')}
            title="Go to List"></Button>
            <TouchableOpacity onPress={() => console.log("DF")}>
            <Text>Hello</Text>
            </TouchableOpacity>
        </View>
    )
}
